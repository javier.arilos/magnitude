FROM openjdk:8-jre-alpine

RUN mkdir -p /magnitude
COPY target/magnitude-0.0.1.jar /magnitude
ADD config.yml /magnitude

EXPOSE 8080

CMD java -jar /magnitude/magnitude-0.0.1.jar server /magnitude/config.yml

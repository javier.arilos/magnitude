package org.milo.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Vector {
    private long magnitude;
    private int directionX;
    private int directionY;
    private String description;

    public Vector() {
        // Jackson deserialization
    }

    public Vector(long id, int directionX, int directionY, String description) {
        this.magnitude = id;
        this.directionX = directionX;
        this.directionY = directionY;
        this.description = description;
    }

    @JsonProperty
    public long getMagnitude() {
        return magnitude;
    }

    @JsonProperty
    public int getDirectionX() {
        return directionX;
    }

    @JsonProperty
    public int getDirectionY() {
        return directionY;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }
}

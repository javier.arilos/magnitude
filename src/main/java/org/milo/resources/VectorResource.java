package org.milo.resources;


import com.codahale.metrics.annotation.Timed;
import java.io.IOException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.PathParam;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.TokenType;
import org.gitlab.api.models.GitlabGroup;
import org.gitlab.api.models.GitlabSession;
import org.gitlab.api.models.GitlabUser;
import org.hibernate.validator.constraints.NotEmpty;
import org.milo.api.Vector;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Optional;

@Path("/vector")
@Produces(MediaType.APPLICATION_JSON)
public class VectorResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public VectorResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    @GET
    @Timed
    @Path("/hello")
    public Vector getVector(@QueryParam("name") Optional<String> name) {
        final String description = String.format(template, name.orElse(defaultName));
        return new Vector(counter.incrementAndGet(), 7, 5, description);
    }

    @GET
    @Timed
    @Path("/gitlab/{private-token}/user")
    public GitlabUser getGitlabUser(
        @PathParam("private-token") @NotEmpty String privateToken
    ) throws IOException {
        GitlabAPI api = getGitlabAPI(privateToken);
        return api.getUser();
    }

    @GET
    @Timed
    @Path("/gitlab/{private-token}/groups")
    public Iterable<GitlabGroup> getGroups(
        @PathParam("private-token") @NotEmpty String privateToken
    ) throws IOException {
        GitlabAPI api = getGitlabAPI(privateToken);
        return api.getGroups();
    }

    @GET
    @Timed
    @Path("/gitlab/{private-token}/group/{group-path}/")
    public GitlabGroup getGroup(
        @PathParam("private-token") @NotEmpty String privateToken,
        @PathParam("group-path") @NotEmpty String groupPath
    ) throws IOException {
        GitlabAPI api = getGitlabAPI(privateToken);

        GitlabGroup group = api.getGroup(groupPath);
        return group;
    }

    private GitlabAPI getGitlabAPI(@NotEmpty String privateToken) {
        return GitlabAPI.connect(
                "https://gitlab.skyscannertools.net",  // "https://gitlab.com",
                privateToken,
                TokenType.PRIVATE_TOKEN
            );
    }
}

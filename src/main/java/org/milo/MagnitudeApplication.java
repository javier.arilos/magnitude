package org.milo;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.milo.health.TemplateHealthCheck;
import org.milo.resources.VectorResource;

public class MagnitudeApplication extends Application<MagnitudeConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MagnitudeApplication().run(args);
    }

    @Override
    public String getName() {
        return "Magnitude";
    }

    @Override
    public void initialize(final Bootstrap<MagnitudeConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final MagnitudeConfiguration configuration,
        final Environment environment) {

        final VectorResource vectorResource = new VectorResource(
            configuration.getTemplate(),
            configuration.getDefaultName()
        );

        final TemplateHealthCheck healthCheck = new TemplateHealthCheck(
            configuration.getTemplate());

        environment.jersey().register(vectorResource);
        environment.healthChecks().register("template", healthCheck);
    }

}

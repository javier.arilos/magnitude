# magnitude

magnitude, together with direction forms a vector. vectors are used to represent movement

# running

How to start the magnitude application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/magnitude-0.0.1.jar server config.yml`
1. Endpoints:
  1. vector endpoint: `curl http://localhost:8080/vector\?name\="Milo"`
  1. Healthcheck: curl http://localhost:8081/healthcheck
  1. metrics endpoint: `curl http://localhost:8081/metrics`

---

To see your applications health enter url `http://localhost:8081/healthcheck`

# history
created with:
```bash
mvn archetype:generate -DarchetypeGroupId=io.dropwizard.archetypes -DarchetypeArtifactId=java-simple -DarchetypeVersion=1.2.2\n
```
